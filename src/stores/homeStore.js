import { defineStore } from 'pinia'

export default defineStore('homeStore', {
  state: () => ({
    live:[
      {
        id:'0',
        title:'',
        img:'https://picsum.photos/359/165?random' 
      },
      {
        id:'1',
        title:'',
        img:'https://picsum.photos/359/164?random' 
      },
      {
        id:'2',
        title:'',
        img:'https://picsum.photos/359/163?random' 
      },
      {
        id:'3',
        title:'',
        img:'https://picsum.photos/359/162?random' 
      },
      {
        id:'4',
        title:'',
        img:'https://picsum.photos/359/161?random' 
      },
    ]
  }),
  actions: {

  }
})

